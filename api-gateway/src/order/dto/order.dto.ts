export class CreateOrderDto {
  account_id: string;
  cart: Cart[];
}
export class Cart {
  product_id: string;
  product_name: string;
  amount: string;
  price: string;
}
