import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { CreateUserRequest } from './account/dto/create-user-request.dto';
import { AuthLoginDto } from './auth/dto/auth-login.dto';
import { JwtService } from '@nestjs/jwt';
import { RpcException } from '@nestjs/microservices';
import { Account } from './account/entity/account.entity';
import { CreateOrderDto } from './order/dto/order.dto';

@Injectable()
export class AppService {
  constructor(
    @Inject('ACCOUNT') private readonly accountClient: ClientProxy,
    @Inject('PRODUCT') private readonly productClient: ClientProxy,
    private jwtService: JwtService,
  ){}

  // -------------------------- ACCOUNT ----------------------------------

  GetUser(id : number){
    return this.accountClient.send({cmd: 'find_account'},id)
  }

  CreateUser(createUserRequest:CreateUserRequest) {
    return this.accountClient.send({cmd: 'user_create' }, createUserRequest);
  }

  getAllUser() {
    return this.accountClient.send({ cmd: 'get_account' }, {});
  }

  // -------------------------- AUTH ----------------------------------

  async auth(authLoginDto: AuthLoginDto) {
    const user = await this.validateUser(authLoginDto);

    const payload = {
      userId: user.account_id,
    };

    return {
      userId:user.account_id,
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(authLoginDto: AuthLoginDto): Promise<Account> {
    const { username, password } = authLoginDto;

    const user = await this.findByUser(username);
    if (!(await user?.validatePassword(password))) {
      throw new RpcException('UnauthorizedException.');
    }

    return user;
  }

  async findByUser(username: string) {
    return await Account.findOne({
      where: {
        username: username,
      },
    });
  }

  // -------------------------- PRODUCT ----------------------------------

  getProduct(id : number){
    return this.productClient.send({cmd: 'get_product'},id)
  }

  getAllProduct(){
    return this.productClient.send({cmd: 'get_all_product'}, {})
  }

  // -------------------------- ORDER ----------------------------------

  createOrder(orderRequest : CreateOrderDto ){
    return this.accountClient.send({cmd: 'order_create' }, orderRequest);
  }

  getOrder(id: number){
    return this.accountClient.send({cmd: 'get_order'}, id);
  }

  getHistory(id: number){
    return this.accountClient.send({cmd: 'get_history'}, id);
  }

  delOrder(id : number){
    return this.accountClient.send({cmd: 'del_order'},id);
  }

}
