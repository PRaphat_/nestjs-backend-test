export class CreateUserEvent {
    constructor(
        public readonly username: string,
        public readonly password: string,
        public readonly fname: string,
        public readonly lname: string,
        public readonly tel: string,
        ) {}
}