import {
  BaseEntity,
  BeforeInsert,
  Entity,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';

@Entity()
export class Account extends BaseEntity {
  @PrimaryGeneratedColumn()
  account_id: number;

  @Column()
  username: string;

  @Column()
  token: string;

  @Column()
  fname: string;

  @Column()
  lname: string;

  @Column()
  tel: string;

  // @BeforeInsert()
  // async hashPassword() {
  //   this.token = await bcrypt.hash(this.token, 10);
  // }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.token);
  }
}
