export class CreateUserRequest {
    username: string;
    password: string;
    fname: string;
    lname: string;
    tel: string;
}