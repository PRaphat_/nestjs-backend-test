import { Body, Controller, Get, Post, Delete, Param, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { CreateUserRequest } from './account/dto/create-user-request.dto';
import { AuthLoginDto } from './auth/dto/auth-login.dto';
import { JwtAuthGuard } from './jwt-auth.guard';
import { CreateOrderDto } from './order/dto/order.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @UseGuards(JwtAuthGuard)
  @Get('/account/:id')
  getUser(@Param('id') id: number) {
    return this.appService.GetUser(id);
  }

  @Post('/account')
  CreateUser(@Body() createUserRequest: CreateUserRequest) {
    return this.appService.CreateUser(createUserRequest);
  }
  
  @Get('/account')
  getAllUser() {
    return this.appService.getAllUser();
  }

  @Post('/auth')
  auth(@Body() authLoginDto: AuthLoginDto) {
    return this.appService.auth(authLoginDto);
  }

  // ------------------------------- ORDER ---------------------------------

  @UseGuards(JwtAuthGuard)
  @Post('/order')
  createOrder(@Body() orderRequest: CreateOrderDto) {
    return this.appService.createOrder(orderRequest);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/order/:id')
  getOrder(@Param('id') id: number) {
    return this.appService.getOrder(id);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/account/:id/history')
  getHistory(@Param('id') id: number){
    return this.appService.getHistory(id);
  }
  
  @UseGuards(JwtAuthGuard)
  @Delete('/order/:id')
  delOrder(@Param('id') id: number) {
    return this.appService.delOrder(id);
  }

  // ------------------------------- PRODUCT --------------------------------
  
  @Get('/product/:id')
  getProduct(@Param('id') id: number) {
    return this.appService.getProduct(id);
  }

  @Get('/product')
  getAllProduct() {
    return this.appService.getAllProduct();
  }
}