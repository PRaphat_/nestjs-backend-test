import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';
import { Account } from './account/entity/account.entity'
import { TypeOrmModule } from '@nestjs/typeorm';
@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: 'MY_SECRET_JWT_KEY',
      })
    }),
    ClientsModule.register([
      {
        name: 'ACCOUNT',
        transport: Transport.TCP,
      },
      {
        name: 'PRODUCT',
        transport: Transport.TCP,
        options: { port: 3001 },
      },
    ]),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'nvsu-account',
      entities: [Account],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Account]),
  ],
  controllers: [AppController],
  providers: [AppService,JwtStrategy],
  // exports:[AppService]
})
export class AppModule {}
