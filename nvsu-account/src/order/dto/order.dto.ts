export class CreateOrderDto {
    account_id: string;
    cart: Cart[];
  }

export class Cart {
    product_id: string;
    product_name: string;
    amount: string;
    price: string;
  }

export class OrderDto {
  order_id: number;
  total : string;
  create_date: string;
  accountAccountId: number;
}

export class OrderDetailDto {
  order_detail_id: number;
  amount: string;
  total_price: string;
  orderOrderId: number;
  product_id: string;
  product_name: string;
}