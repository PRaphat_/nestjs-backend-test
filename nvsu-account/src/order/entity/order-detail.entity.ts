import {
    BaseEntity,
    BeforeInsert,
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
  } from 'typeorm';

  import {Order} from "./order.entity"; 

  @Entity()
  export class OrderDetail {
    @PrimaryGeneratedColumn()
    order_detail_id: number;
    
    @Column()
    product_id: string;

    @Column()
    product_name: string;

    @Column()
    amount: string;
    
    @Column()
    total_price: string;

    @ManyToOne(() => Order, (order) => order.orderdetails) 
    order: Order;

    @Column()
    orderOrderId: number;

  }
  