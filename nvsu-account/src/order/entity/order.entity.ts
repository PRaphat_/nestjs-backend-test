import {
    BaseEntity,
    BeforeInsert,
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    ManyToOne,
    OneToMany,
  } from 'typeorm';

  import {Account} from "../../account/entity/account.entity"; 
  import {OrderDetail} from "./order-detail.entity"; 
  @Entity()
  export class Order {
    @PrimaryGeneratedColumn()
    order_id: number;

    @Column()
    total: string;
  
    @Column()
    @CreateDateColumn()
    create_date: Date;

    @ManyToOne(() => Account, (account) => account.orders) 
    account: Account;

    @OneToMany(() => OrderDetail, (orderdetail) => orderdetail.order)
    orderdetails: OrderDetail[]

    @Column()
    accountAccountId: number;

  }
  