import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './jwt.strategy';

import { OrderDetail } from './order/entity/order-detail.entity'
import { Order } from './order/entity/order.entity'
import { Account } from './account/entity/account.entity'

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: () => ({
        secret: 'MY_SECRET_JWT_KEY',
      })
    }),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '',
      database: 'nvsu-account',
      entities: [Account,Order,OrderDetail],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Account,Order,OrderDetail]),
  ],
  controllers: [AppController],
  providers: [JwtStrategy,AppService],
  exports:[TypeOrmModule],
})
export class AppModule {
}
