import { Controller } from '@nestjs/common';
import { EventPattern,MessagePattern } from '@nestjs/microservices';
import { AppService } from './app.service';
import { CreateUserEvent } from './account/event/create-user.event';
import { AuthLoginDto } from './auth/dto/auth-login.dto';
import { CreateOrderDto } from './order/dto/order.dto';

@Controller()
export class AppController {
  constructor( private readonly appService: AppService) {}

  // @UseGuards(JwtAuthGuard)
  @MessagePattern({ cmd: 'find_account' })
  getAccount(id : number){
    return this.appService.getAccount(id);
  }

  @MessagePattern({cmd: 'user_create' })
  handleUserCreated(data: CreateUserEvent) {
    return this.appService.handleUserCreated(data);
  }

  @MessagePattern({ cmd: 'get_account' })
  getAllAccount() {
    return this.appService.getAllAccount();
  }

  @MessagePattern({ cmd: 'auth' })
  login(authLoginDto: AuthLoginDto) {
    return this.appService.login(authLoginDto);
  }

  @MessagePattern({ cmd: 'order_create'})
  createOrder(order : CreateOrderDto){
    return this.appService.createOrder(order);
  }

  @MessagePattern({ cmd: 'get_order'})
  getOrder(id : number){
    return this.appService.getOrder(id);
  }

  @MessagePattern({ cmd: 'get_history'})
  getHistory(id:number){
    return this.appService.getHistory(id);
  }

  @MessagePattern({ cmd: 'del_order'})
  delOrder(id:number){
    return this.appService.delOrder(id);
  }
  
}
