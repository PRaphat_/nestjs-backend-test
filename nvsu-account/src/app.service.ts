import { Injectable } from '@nestjs/common';
import { CreateUserEvent } from './account/event/create-user.event';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAccDto } from './account/dto/create-account.dto';
import { AuthLoginDto } from './auth/dto/auth-login.dto';
import { RpcException } from '@nestjs/microservices';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import {
  CreateOrderDto,
  OrderDto,
  OrderDetailDto,
} from './order/dto/order.dto';
import { Account } from './account/entity/account.entity';
import { Order } from './order/entity/order.entity';
import { OrderDetail } from './order/entity/order-detail.entity';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Account)
    private accountRepository: Repository<Account>,

    @InjectRepository(Order)
    private orderRepository: Repository<Order>,

    @InjectRepository(OrderDetail)
    private detailRepository: Repository<OrderDetail>,

    private jwtService: JwtService,
  ) {}

  //===================================== ACCOUNT ==================================================

  async getAccount(id) {
    return await this.accountRepository.find({
      select: {
        account_id:true,
        username: true,
        fname: true,
        lname: true,
        tel: true,
      },
      where: {
        account_id: id,
      },
    });
  }

  async handleUserCreated(data: CreateUserEvent) {
    const check = await this.accountRepository.find({
      where: {
        username: data.username,
      },
      take: 1,
    });

    if (check.length < 1) {
      let hpash = await bcrypt.hash(data.password, 10);
      let query = new CreateAccDto();
      query.username = data.username;
      query.token = hpash;
      query.fname = data.fname;
      query.lname = data.lname;
      query.tel = data.tel;
      const account = await this.accountRepository.save(query);
      delete account.token;
      return account;
    } else {
      // throw new RpcException('Username Already Exists.');
      return {
        Status: 'Error',
        Msg: 'Username Already Exists.',
      };
    }
  }

  async getAllAccount() {
    return await this.accountRepository.find({
      select: {
        account_id:true,
        username: true,
        fname: true,
        lname: true,
        tel: true,
      },
    });
  }

  //===================================== AUTH ==================================================

  async login(authLoginDto: AuthLoginDto) {
    const user = await this.validateUser(authLoginDto);

    const payload = {
      userId: user.account_id,
    };

    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async validateUser(authLoginDto: AuthLoginDto): Promise<Account> {
    const { username, password } = authLoginDto;

    const user = await this.findByUser(username);
    if (!(await user?.validatePassword(password))) {
      throw new RpcException('UnauthorizedException.');
    }

    return user;
  }

  async findByUser(username: string) {
    return await Account.findOne({
      where: {
        username: username,
      },
    });
  }

  async validate(authLoginDto: AuthLoginDto): Promise<Account> {
    const { username, password } = authLoginDto;
    const user = await this.findByUser(username);
    if (!(await user?.validatePassword(password))) {
      throw new RpcException('UnauthorizedException.');
    }

    return user;
  }

  //===================================== ORDER ==================================================

  async createOrder(order: CreateOrderDto) {
    let account_id = order.account_id;
    let Order = new OrderDto();
    Order.accountAccountId = parseInt(account_id);

    Order.total = '0';
    const addOrder = await this.orderRepository.save(Order);
    const order_id = addOrder.order_id;
    const cart = order.cart;
    let total_price = 0;

    for (let i = 0; i < cart.length; i++) {
      let orderDetail = new OrderDetailDto();
      let total = parseInt(cart[i].amount) * parseInt(cart[i].price);
      total_price += total;
      orderDetail.amount = cart[i].amount;
      orderDetail.total_price = total.toString();
      orderDetail.product_id = cart[i].product_id;
      orderDetail.product_name = cart[i].product_name;
      orderDetail.orderOrderId = order_id;
      await this.detailRepository.save(orderDetail);
    }
    Order.total = total_price.toString();
    const update = await this.orderRepository.save(Order);
    return update;
  }

  async getOrder(id: number) {
    const Order = await this.orderRepository.find({
      where: {
        order_id: id,
      },
    });
    const OrderDetail = await this.detailRepository.find({
      where: {
        orderOrderId: id,
      },
    });
    const res = [...Order, OrderDetail];
    return res;
  }

  async getHistory(id: number) {
    const res = await this.orderRepository.find({
      where: {
        accountAccountId: id,
      },
    });
    return res;
  }

  async delOrder(id: number) {
    const OrderDetail = await this.detailRepository.find({
      where: {
        orderOrderId: id,
      },
    });

    for(let i = 0; i < OrderDetail.length; i++) {
      await this.detailRepository.delete(OrderDetail[i].order_detail_id);
    }
    await this.orderRepository.delete(id);
    return {
      Status: 'Success',
      Msg: 'Delete order success.',
    };
  }
}
