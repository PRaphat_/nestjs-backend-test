export class CreateAccDto {
    id:number;
    username:string;
    token:string;
    fname:string;
    lname: string;
    tel:string;
}