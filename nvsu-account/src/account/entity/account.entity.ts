import {
  BaseEntity,
  BeforeInsert,
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Order } from "../../order/entity/order.entity"; 

@Entity()
export class Account extends BaseEntity {
  @PrimaryGeneratedColumn()
  account_id: number;

  @Column()
  username: string;

  @Column()
  token: string;

  @Column()
  fname: string;

  @Column()
  lname: string;

  @Column()
  tel: string;

  @OneToMany(() => Order, (order) => order.account) 
  orders: Order[]; 

  // @BeforeInsert()
  // async hashPassword() {
  //   this.token = await bcrypt.hash(this.token, 8);
  // }

  async validatePassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.token);
  }
}
