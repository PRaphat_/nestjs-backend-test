import { Body, Controller, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern,MessagePattern } from '@nestjs/microservices';
import { AddProductDto } from './product/dto/add-product.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Post()
  addProduct(@Body() addProductDto: AddProductDto){
    return this.appService.addProduct(addProductDto);
  }

  @MessagePattern({ cmd: 'get_product'})
  getProduct(id : number){
    return this.appService.getProduct(id);
  }

  @MessagePattern({ cmd: 'get_all_product'})
  getProducts(){
    return this.appService.getProducts();
  }
}
