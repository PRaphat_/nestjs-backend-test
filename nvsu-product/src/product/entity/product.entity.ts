import {
    BaseEntity,
    Entity,
    Column,
    PrimaryGeneratedColumn,
    CreateDateColumn,
  } from 'typeorm';

  @Entity()
  export class Product {
    @PrimaryGeneratedColumn()
    product_id: number;
  
    @Column()
    product_name: string;
  
    @Column('text')
    product_desc: string;
 
    @Column()
    product_price: string;
  
    @Column()
    @CreateDateColumn()
    create_date: Date;
  
  }
  