export class AddProductDto {
    product_id:number;
    product_name:string;
    product_desc:string;
    product_stock:string;
    product_price: string;
    create_date:string;

}