import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { RpcException } from '@nestjs/microservices';
import { Product } from './product/entity/product.entity';
import { AddProductDto } from './product/dto/add-product.dto'

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(Product)
    private productRepository: Repository<Product>,
  ) {}

  async addProduct(data:AddProductDto){
    return await this.productRepository.save(data)
  }

  async getProduct(id) {
    return await this.productRepository.find({
      where: {
        product_id: id,
      },
    });
  }

  async getProducts(){
    return await this.productRepository.find();
  }

}
